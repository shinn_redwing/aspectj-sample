package by.sample.aspectj;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class LoggerAspect {
    private static final String GREETING = "Hello from AspectJ!";
    private static final String LINE = "-----------------------";

    @Pointcut("call(* org.slf4j.Logger.info(..)) && !within(by.sample.aspectj.LoggerAspect)")
    public void callAt() {}

    @Around("callAt()")
    public Object before(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println(LINE);
        System.out.println(GREETING);
        System.out.println(LINE);
        Object result = joinPoint.proceed();
        System.out.println(LINE);

        return result;
    }
}
